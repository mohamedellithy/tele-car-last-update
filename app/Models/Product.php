<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = [
        'name', 'category_id', 'user_id' ,'price' ,'discount', 'details' ,'num_views','lat','lan','whats_number','call_phone'
    ];
    
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

   
}
