<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Order extends Model
{
    protected $fillable = [
        'status', 'category_id', 'user_id' 
    ];

    public function product(){
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function custom_products(){
    	return $this->belongsTo(Product::class, 'product_id');
    }
}
