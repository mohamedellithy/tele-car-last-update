<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    "Category" => Category::class,
    "Setting" => Setting::class,
    "Product" => Product::class,
]);
class Image extends Model
{
    protected $fillable = [
        'imagable_id','imagable_type','name','img_url'
    ];

    public function imagable()
    {
        return $this->morphTo();
    }
}
