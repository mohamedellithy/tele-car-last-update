<?php

namespace App\Models;

use App\User;
use App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
    //
    
    protected $fillable = [
        'message', 'product_id', 'user_id' ,'merchant_id' ,'type_message','read_notifiy',
    ];
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function merchant(){
        return $this->belongsTo(User::class, 'merchant_id');
    }
    
    public function product(){
    	return $this->belongsTo(Product::class, 'product_id');
    }

}
