<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'name', 'description', 'about_us' ,'policy'
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }
}
