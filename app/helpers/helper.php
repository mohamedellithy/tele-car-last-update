<?php

function callback_data(bool $status = true, string $msg = '', array $data = [], array $errors = [], int $code = 200)
{
    return response()->json([
        'status' => $status,
        'msg' => $msg,
        'data' => $data,
        'errors' => $errors,
    ], $code);
}

function firebaseNotification($ids,$topic_to=null,$title, $message, $model)
{
    define('API_ACCESS_KEY', 'AIzaSyAa6u0Gy4d-M4x5Er-ByqXP48SGIz90D_Y');
    $msg = array
    (
        'title' => $title,
        'body' => $message,
        'object' => $model,
        'sound' => 'default'
    );
    if(empty($ids) || $ids==null ){
        $fields = array(
                'to' => $topic_to,
                'notification' => $msg,
                'data' => $msg
        ); 
    }
    elseif( ($topic_to==null) || ( empty($topic_to) )  ){
        $fields = array(
            'registration_ids' => $ids,
            'notification' => $msg,
            'data' => $msg
       );
        
    }
    
    $headers = array
    (
        'Authorization:key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    #Send Reponse To FireBase Server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    //echo $result;
    curl_close($ch);
    // end of firebase connection
}
