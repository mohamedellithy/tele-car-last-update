<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class userController extends Controller
{
    
    function show_all_users(){
    	$all_data_user = User::all();

    	return view('admin.pages.users.index')->with('all_users',$all_data_user);
    }

    function delete_user($user_id){
    	  User::where('id',$user_id)->delete();
          return redirect()->route('admin.show-users')->with('success',true);
    }

    function delete_users_selected(){
    	 if(!empty($request->selected_user)){
               User::whereIN('id',$request->selected_user)->delete();
               return back()->with('success',true);
           }

           return back();
    }

    function edite_user_data($user_id){
    	  $user_data = User::where('id',$user_id)->first();
          return view('admin.pages.users.edite')->with('user_data',$user_data);
    }

    function show_users_for_vendor(){

        $all_data_user = User::all();
        return view('vendor.pages.users.index')->with('all_users',$all_data_user);

    }

    
}
