<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use App\Category;
use File;
use Storage;


class adminController extends Controller
{
    function add_vendor_data_type_post(Request $request){
        $this->validate($request,[
                 'name_vendor'=>'required|string|max:255',
                 'email'=>'required|email|max:255|unique:users',
                 'phone'=>'numeric|min:12|unique:users',
                 'password_vendor'=>'required|string|min:6|same:password_confirmation',
               
        ]);

        $add_vendors=new User();
        $add_vendors->name = $request->name_vendor;
        $add_vendors->email = $request->email;
        $add_vendors->phone = $request->phone;
        $add_vendors->password = bcrypt($request->password_vendor);
        $add_vendors->save();
        $role = config('roles.models.role')::where('name', '=', 'vendor')->first();  //choose the default role upon user creation.
        $add_vendors->attachRole($role);
        return back()->with('success',true);
    }

    function edite_vendor_data($vendor_id){
        $get_vendor_data=User::where('id',$vendor_id)->first();
        if(!empty($get_vendor_data)){
            $vendor_data = $get_vendor_data;
        }
        else{
            $vendor_data = null;
        }

        return view('admin.pages.vendors.add')->with('vendor_data',$vendor_data);
    }

    function edite_vendor_data_post_type(Request $request){
        $check_if_email_not_used_befor=User::where('id',$request->vendor_id)->pluck('email');
        $check_if_phone_not_used_befor=User::where('id',$request->vendor_id)->pluck('phone');
        $validate_data=[
                         'name_vendor'=>'required|string|max:255',
                      ];
        if($check_if_email_not_used_befor[0]!=$request->email){
            $email_validate  =array( 'email'=>'numeric|min:12|unique:users');
             $all_validation = array_merge($validate_data, $email_validate);
        }
        elseif($check_if_email_not_used_befor[0]==$request->email){
            $email_validate =array( 'email'=>'required|email|max:255');
            $all_validation = array_merge($validate_data,$email_validate);
        }

        if($check_if_phone_not_used_befor[0]!=$request->phone){
            $phone_validate  =array( 'phone'=>'numeric|min:12|unique:users');
             $all_validation = array_merge($validate_data, $phone_validate);
        }
        elseif($check_if_phone_not_used_befor[0]==$request->phone){
            $phone_validate =array( 'phone'=>'numeric|min:12');
            $all_validation = array_merge($validate_data,$phone_validate);
        }
       
        $this->validate($request,$all_validation);

          $edite_vendor_data=User::where('id',$request->vendor_id)->update(['name'=>$request->name_vendor,
                'email'=>$request->email,
                'phone'=>$request->phone]); 
           return back()->with('success',true);  
    }

    function delete_vendor_data($vendor_id){
        User::where('id',$vendor_id)->delete();
        return redirect()->route('admin.show-vendors')->with('success',true);
    }

    function delete_selected_vendor(Request $request){

    	   if(!empty($request->selected_vendor)){
               User::whereIN('id',$request->selected_vendor)->delete();
               return back()->with('success',true);
           }

           return back();

    }

    
}
