<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

     public function index()
    {
        /** * get user authentaction data **/
        $user=Auth::user();

        /**  * check if user auth  **/
        if($user){
            
            /**  * redirect to page of admin **/
            if ($user->hasRole('admin')) { 
                return redirect()->route('admin.home');
            }


            /** * redirect to page of user  **/
            elseif($user->hasRole('user')){
                return view('home');
                
            }
            /** * redirect to page of vendor **/
            elseif($user->hasRole('vendor')){
                 return redirect()->route('vendor.dashboard');
            }
        }
        else
        {
           /** * or login  **/
           return  redirect()->route('login');   
        }
    }

    public function admin()
    {
        return view('admin.index');
        
    }

    public function vendor()
    {
        return view('vendor.index');
    }
}
