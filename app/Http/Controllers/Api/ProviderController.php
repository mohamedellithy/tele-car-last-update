<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\notification;
use App\Models\Order;
use App\User;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    public function myProducts()
    {
        $user_id = auth()->id();
        $products = Product::where('user_id', $user_id)->with('image')->get();

        return callback_data(true, 'this is all products', compact('products'));
    }

    public function deleteProduct(Request $request)
    {
        $image_name = Product::where('id',$request->id)->first()->image->name;
        
        $deleted = Product::find($request->id)->delete();
        Image::where('imagable_id',$request->id)->delete();
        if(!empty($image_name)){
    	    \File::delete(public_path("product_images/$image_name"));  	
    	}

        return callback_data(true, 'this is all products', compact('deleted'));
    }
    
    
    function handle_product_images($image){
        $orginalNameImage=null;
        if( !empty($image) ){
            $product_image_handle = $image;
            $orginalNameImage = time().'_'.$product_image_handle->getClientOriginalName();
            $product_image_handle->move(public_path('product_images'),$orginalNameImage);  
        }
         return $orginalNameImage;
    }

    public function addProduct(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            // code 422
            return callback_data(false, 'there are errors', [], $errors, 200);
        }
        
        $request['user_id'] = Auth::user()->id;

        $product = Product::create($request->all());
        
        $orginalNameImage = $this->handle_product_images($request->file('product_image'));
        
        $add_image = new Image();
        $add_image->imagable_id = $product->id;
        $add_image->imagable_type = 'Product';
        $add_image->name = $orginalNameImage;
        $add_image->img_url = asset("product_images/$orginalNameImage");
        $add_image->save();
        
        
        
        $all_products=Product::where('id',$product->id)->with('image','user')->get();
        if(!empty($product->discount )){
            
            /* save notification on database */
            $add_new_offer = new notification();
            $add_new_offer->product_id = $product->id;
            $add_new_offer->message = 'NewOffer';
            $add_new_offer->save();
            
            /*  handle message of offer */
            $new_offer = $product->name.'  تخفيض   '.$product->discount.' درهم  '.$product->price.' درهم' ;
            
            /* send notification */
            firebaseNotification(null,'/topics/New_Offer','NewOffer',$new_offer,$all_products);
        }
        
        return callback_data(true, 'this is all products', compact('all_products'));
    }

    public function validator(array $data): ?Validator
    {
        $fields = [
            'name' => 'required|string|max:255',
            'category_id' => 'required|integer',
            'user_id' => 'required|integer',
            'price' => 'required',
            'discount' => 'required',
            'details' => 'required',
        ];

        $messages = [
            '*.required' => 'هذا الحقل مطلوب',
        ];

        return \Validator::make($data, $fields, $messages);
    }

    public function allCategories()
    {
        $categories = Category::with('image')->get();
        return callback_data(true, 'this is all categories', compact('categories'));
    }

    public function categoryProducts($id)
    {
        $products = Product::with('user','image')->where('category_id', $id)->get();
        return callback_data(true, 'this is all products', compact('products'));
    }

    public function countOrders()
    {
        $user_id = auth()->id();
        $products_ids = Product::where('user_id', $user_id)->pluck('id')->toArray();
        $count = Order::whereIn('product_id', $products_ids)->count();

        return callback_data(true, 'this is all categories', compact('count'));
    }

    public function openOrders()
    {
        $user_id = auth()->id();
        $products_ids = Product::where('user_id', $user_id)->pluck('id')->toArray();
        $orders = Order::with('user','product')->whereIn('product_id', $products_ids)->where('status', 'pending')->get();

        return callback_data(true, 'this is all categories', compact('orders'));
    }
    
    public function getSearchWords($search){
        $search_words =$search;
        $search_products = Product::where('name','LIKE',"%{$search_words}%")->orWhere('details','LIKE',"%{$search_words}%")->with('image','user')->get();
        return callback_data(true, 'this is all search', compact('search_products'));
    }
    
    public function getcities(){
        $cities = array(
                    array(
                        'id'=>'1',
                        'city_name'=>'دبى',
                    ),
                    array(
                        'id'=>'2',
                        'city_name'=>'ابوظبى',
                    ),
                    array(
                        'id'=>'3',
                        'city_name'=>'الشارقة',
                    ),
                    array(
                        'id'=>'4',
                        'city_name'=>'العين',
                    ),
                    array(
                        'id'=>'5',
                        'city_name'=>'رأس الخيمة',
                    ),
                    array(
                        'id'=>'6',
                        'city_name'=>'عجمان',
                    ),
                    array(
                        'id'=>'7',
                        'city_name'=>'الفجيرة',
                    ),
                    array(
                        'id'=>'8',
                        'city_name'=>'أم القيوين',
                    ),
                    array(
                        'id'=>'9',
                        'city_name'=>'خورفكان',
                    ),
                    array(
                        'id'=>'10',
                        'city_name'=>'دبا الحصن',
                    ),
                    
                        
                );
        
        return callback_data(true, 'this is all cities', compact('cities'));
        
    }
}
