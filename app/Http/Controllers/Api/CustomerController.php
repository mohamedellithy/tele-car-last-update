<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Image;

class CustomerController extends Controller
{
    public function getProductByDiscount()
    {
        $products = Product::with('user','image')->orderBy('discount', 'desc')->get();

        return callback_data(true, 'this is all products', compact('products'));
    }

    public function getProductByViews()
    {
        $products = Product::with('user','image')->orderBy('num_views', 'desc')->get();

        return callback_data(true, 'this is all products', compact('products'));
    }

    public function notifications()
    {
        $notifications = [];

        return callback_data(true, 'this is all notifications', compact('notifications'));
    }
}
