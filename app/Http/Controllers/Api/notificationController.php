<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use App\Models\Product;
use App\Models\notification;
use App\User;

class notificationController extends Controller
{
    public function send_notifiy_to_merchant(Request $request){
        
        $request['type_message'] = 1;
        $request['user_id'] = Auth::user()->id;
        
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            // code 422
            return callback_data(false, 'there are errors', [], $errors, 200);
        }
        
        $notification = notification::create($request->all());
        
       $message_notification = array('id'     =>$notification->id,
                                     'type_notifiy' =>'message',
                                     'message'=>$request['message'],
                                     'user_id'=>$request['user_id'],
                                     'merchant_id'=>$request['merchant_id'],
                                     'prdouct_id' =>$request['product_id'],
                                     'from'       => Auth::user()->name,
                                     'to'         =>User::where('id',$request['merchant_id'])->pluck('name')[0],
                                ); 
        
        $token_ids = User::where('id',$request['merchant_id'])->pluck('fcm_token')->toArray();
        
        firebaseNotification($token_ids,null,'New Message','New Message',$message_notification);
        return callback_data(true, 'notification send to merchant', compact('message_notification'));
        
         
    }
    
    
    public function send_notifiy_to_user(Request $request){
        
        $request['type_message'] = 0;
        $request['merchant_id'] = Auth::user()->id;
        
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            // code 422
            return callback_data(false, 'there are errors', [], $errors, 200);
        }
        
        $notification = notification::create($request->all());
        
       $message_notification = array('id'     =>$notification->id,
                                     'type_notifiy' =>'message',
                                     'message'=>$request['message'],
                                     'merchant_id'=>$request['merchant_id'],
                                     'user_id'=>$request['user_id'],
                                     'prdouct_id' =>$request['product_id'],
                                     'from'       =>Auth::user()->name,
                                     'to'         =>User::where('id',$request['user_id'])->pluck('name')[0],
                                ); 
        
        $token_ids = User::where('id',$request['user_id'])->pluck('fcm_token')->toArray();
        
        firebaseNotification($token_ids,null,'New Message','New Message',$message_notification);
        return callback_data(true, 'notification send to user ', compact('message_notification'));
        
         
             
    }
    
    
    
    public function validator(array $data): ?Validator
    {
        $fields = [
            'message' => 'required',
            'merchant_id' => 'required|integer',
            'user_id' => 'required|integer',
            'product_id' => 'required|integer',
        ];

        $messages = [
            '*.required' => 'هذا الحقل مطلوب',
        ];

        return \Validator::make($data, $fields, $messages);
    }
    
    public function get_notification($user_id){
        
        $notification = notification::where(['user_id'=>$user_id,'merchant_id' =>Auth::user()->id ])
                        ->orWhere(['merchant_id'=>$user_id,'user_id'=>Auth::user()->id ])->get();
        $all_notification = $this->handle_notifications_messages( $notification );
        return callback_data(true, 'notification send to user ', compact('all_notification'));
        
    }
    
   /* public function get_all_notifications(){
        $all_notification_add_products = Product::where('notify_user',1)->with('image')->get();
        return callback_data(true, 'notification send to user ', compact('all_notification_add_products'));
    }*/
    
    public function read_notification($notifiy_id){
        $notification = notification::where('id',$notifiy_id)->update(['read_notifiy'=>1]);
        return callback_data(true, 'notification readed ', compact('notification'));
    }
    
    public function get_all_notifications(){
        $notification = notification::all();
        $all_notification = $this->handle_notifications_messages( $notification );
        return callback_data(true, 'all notification ', compact('all_notification'));
    }
    
    public function handle_notifications_messages($all_notifiy_messages){
        
        $all_messages = array();
        foreach($all_notifiy_messages as $message){
            if($message->user_id!=null){
              $user_name = User::where('id',$message->user_id)->get()->pluck('name')[0];
              $merchant_name = User::where('id',$message->merchant_id)->get()->pluck('name')[0];
              $all_messages[]=array( 'id'     =>$message->id,
                                     'message'=>$message->message,
                                     'product_id'=>$message->product_id,
                                     'user_id'=>$message->user_id,
                                     'merchant_id'=>$message->merchant_id,
                                     'from'=>( ($message->type_message == 0 )?$merchant_name : $user_name),
                                     'to'  =>( ($message->type_message == 0 )?$merchant_name : $user_name),
                                     'read' => $message->read_notify,
                                     'send_at'=>$message->created_at->format('U'),
                                ); 
            }
            elseif($message->user_id==null){
              
              $all_messages[]=array( 'id'     =>$message->id,
                                     'message'=>$message->message,
                                     'product_id'=>$message->product_id,
                                     'user_id'=>null,
                                     'merchant_id'=>null,
                                     'from'=>null,
                                     'to'  =>null,
                                     'read' => $message->read_notify,
                                     'timestamp'=>$message->created_at->format('U'),
                                     'product'=>Product::where('id',$message->product_id)->with('user')->get(),
                                ); 
            }
        }        
        return $all_messages;
    }
}
