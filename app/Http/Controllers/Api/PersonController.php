<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
//use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;

class PersonController extends Controller
{
    // Auth API
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = auth()->user();
            if (request()->has('token')) {
                /*$user->update(['token' => request('token')]);*/
                User::where('id',Auth::user()->id)->update(['token' => request('token')]);
            }
            if(request()->has('fcm_token')){
               User::where('id',$user->id)->update(['fcm_token'=>request('fcm_token')]);
            }
            if ($user->image != null) {
                $user['image_url'] = asset($user->image->name);
            }
            unset($user['image']);

            $user['token'] = $user->createToken('MyApp')->accessToken;
            
            
            if($user->hasRole('vendor')){
                $user['type_user'] = 'Merchant'; 
            }
            elseif($user->hasRole('user'))
            {
              $user['type_user'] = 'Customer';                
            }
            elseif($user->hasRole('admin'))
            {
                $user['type_user'] = 'admin';
            }
            
            return callback_data(true, 'user is logged', compact('user'));
        } else {
            //code 401
            return callback_data(false, 'Unauthorised', [], [], 200);
        }
    }

    public function validator(array $data): ?Validator
    {
        $fields = [
            'name' => 'required|string|max:255'
        ];

        $fields['email'] = !empty($data['id']) ?
            'required|email|max:191|unique:users,email,' . $data['id'] : 'required|email|max:191|unique:users';
        $fields['phone'] = !empty($data['id']) ?
            'required|string|max:20|unique:users,phone,' . $data['id'] : 'required|string|max:191|unique:users';
        $fields['password'] = !empty($data['id']) ?
            'sometimes|string|min:6|confirmed' : 'required|string|min:6|confirmed';

        $messages = [
            '*.required' => 'هذا الحقل مطلوب'
        ];

        return \Validator::make($data, $fields, $messages);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            //code 422
            return callback_data(false, 'there are errors', [], $errors, 200);
        }
        
        $request['password'] = bcrypt($request['password']);
        $user = User::create($request->all());
        $user['token'] = $user->createToken('MyApp')->accessToken;
        if ($user->image != null) {
            $user['image_url'] = asset($user->image->name);
        }
        /* add lat and lan */
        $user['lat'] = $request['lat'];
        $user['lan'] = $request['lan'];
        
        if( empty( $request['type_user'] ) || ($request['type_user']=='Customer') ){
              $role = config('roles.models.role')::where('name', '=', 'User')->first();  //choose the default role upon user creation.
              $user->attachRole($role);    
              $user['type_user'] = 'Customer';
        }
        elseif( !empty( $request['type_user'] ) && ($request['type_user']=='Merchant') ){
              $role = config('roles.models.role')::where('name', '=', 'vendor')->first();  //choose the default role upon user creation.
              $user->attachRole($role);    
              $user['type_user'] = 'Merchant';
        }
        
       
       
        
        /* add fcm_token */
        $user['fcm_token'] = $request['fcm_token'];
        
        unset($user['image']);
        return callback_data(true, 'user is registered', compact('user'));
    }
}
