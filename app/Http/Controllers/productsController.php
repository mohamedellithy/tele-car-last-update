<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\notification;
use App\Models\Image;
use App\User;
use Auth;
use File;

class productsController extends Controller
{
    
    function show_products(){
        $all_products_vendor_have = Product::where('user_id',Auth::user()->id )->get();
    	return view('vendor.pages.products.index')->with('all_products',$all_products_vendor_have);
    }

    function add_products(){
        $all_category=Category::all();
    	return view('vendor.pages.products.add')->with('all_categories',$all_category);
    }
    
     function handle_product_images($image){
        $orginalNameImage=null;
        if( !empty($image) ){
            $product_image_handle = $image;
            $orginalNameImage = time().'_'.$product_image_handle->getClientOriginalName();
            $product_image_handle->move(public_path('product_images'),$orginalNameImage);  
        }
         return $orginalNameImage;
    }

    function add_products_post_type(Request $request){
        $this->validate($request,[
         	     'product_name'=>'required|string|max:255',
                 'full_description'=>'string|max:255',
                 'product_price'=>'required|numeric',
                 'product_image'=>'mimes:jpg,png,jpeg', 
                 
                  //product_category
         	]);  
        
        $orginalNameImage = $this->handle_product_images($request->file('product_image'));

        $add_data_in_table = new Product();
        $add_data_in_table->name             = $request->product_name;
        $add_data_in_table->category_id      = $request->product_category;
        $add_data_in_table->details          = $request->full_description;
        $add_data_in_table->price            = $request->product_price;
        $add_data_in_table->user_id          = Auth::user()->id;
        $add_data_in_table->call_phone       = $request->call_phone;
        $add_data_in_table->whats_number     = $request->whats_number;
        $add_data_in_table->discount         = (!empty($request->product_discount)?intval($request->product_discount):0);
        $add_data_in_table->save();
        
        $add_image = new Image();
        $add_image->imagable_id = $add_data_in_table->id;
        $add_image->imagable_type = 'Product';
        $add_image->name = $orginalNameImage;
        $add_image->img_url = asset("product_images/$orginalNameImage");
        $add_image->save();
        
        // $all_user_send_notification = User::where('fcm_token','!=',null)->pluck('fcm_token')->toArray(); 
        
        if(!empty($add_data_in_table->discount )){
            
             /* save notification on database */
            $add_new_offer = new notification();
            $add_new_offer->product_id = $add_data_in_table->id;
            $add_new_offer->message = 'NewOffer';
            $add_new_offer->save();
            
            /* handle message of offer */
            $new_offer = $add_data_in_table->name.' تخفيض '.$add_data_in_table->discount.' درهم '.$add_data_in_table->price.' درهم' ;
            
            /* send message on firebase */
            firebaseNotification(null,'/topics/New_Offer','NewOffer',$new_offer,$add_data_in_table);

        }
        
        return back()->with('success',true);

    }

   function delete_product_data($product_id){
          $image_name = Product::where('id',$product_id)->first()->image->name;
          Product::where('id',$product_id)->delete();
          if(!empty($image_name)){
    	    \File::delete(public_path("product_images/$image_name"));  	
    	    Image::where('imagable_id',$product_id)->delete();
         }
          return redirect()->route('vendor.show-products')->with('success',true);
    }

    function edite_product($product_id){
    	$get_product_data=Product::where('id',$product_id)->first();
    	$all_categories=Category::all();
        if(!empty($get_product_data)){
            $product_data = $get_product_data;
        }
        else{
            $product_data = null;
        }

        return view('vendor.pages.products.add')->with('all_categories',$all_categories)->with('product_data',$get_product_data);
    }

    function edite_product_post(Request $request){
    	    $this->validate($request,[
         	     'product_name'=>'required|string|max:255',
                 'full_description'=>'string|max:255',
                 'product_price'=>'required|numeric',
                 
                  //product_category
         	]);  
         	
         	$check_if_image_not_used_befor=Image::where('imagable_id',$request->product_id)->pluck('name');
	        if($check_if_image_not_used_befor[0]!=$request->file('product_image') ){
	            $image_update = $this->handle_product_images($request->file('product_image'));
	            if(empty($request->file('product_image'))){
	                $image_update = $request->old_value;
	            }
	        }
	        elseif($check_if_image_not_used_befor[0]==$request->file('product_image') ){
	            $image_update = $check_if_image_not_used_befor[0];
	        }

    	 $edite_product_data = Product::where('id',$request->product_id)->update([
    	        	    'name'=>$request->product_name,
    	        	    'details'=>$request->full_description,
    	        	    'price'=>$request->product_price,
    	        	    'category_id'=>$request->product_category,
    	        	    'call_phone' => $request->call_phone,
                        'whats_number' => $request->whats_number,
    	        	    'discount'=>(!empty($request->product_discount)?intval($request->product_discount):0),
	                ]); 
	   
	    Image::where('imagable_id',$request->product_id)->update(['name'=>$image_update,'img_url'=>asset("product_image/$image_update")]); 
	        
	        
	    return back()->with('success',true); 
    }

    function delete_products_selected(Request $request){
    	  if(!empty($request->selected_products)){
    	       $all_selected_products = Product::whereIN('id',$request->selected_products)->get();
               Product::whereIN('id',$request->selected_products)->delete();
               foreach ($all_selected_products as $all_selected_product) {
           	   	    $image_name = $all_selected_product->image->name;
           	   	    if(!empty($image_name)){
    		    	    \File::delete(public_path("product_images/$image_name"));  	
    		    	}

       	        }
       	        Image::whereIN('imagable_id',$request->selected_products)->delete();
               return back()->with('success',true);
           }
           
           return back();
    }

    function show_message_user($messenger_id){
        return view('vendor.departments.user_messages')->with('messenger_id',$messenger_id);
    }
    
    function delete_image_data($type_id){
        Image::where('imagable_id',$type_id)->update(['name'=>null,'img_url'=>null]);
        return back();
    }
}
