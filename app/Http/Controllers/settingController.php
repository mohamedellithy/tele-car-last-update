<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use Auth;

class settingController extends Controller
{
    //

    function add_about_us(Request $request){
        $insert_about_us = new Setting();
        $insert_about_us->about_us    = $request->about_us;
        if(Auth::user()->hasRole('admin')){
            $insert_about_us->description = 'admin';
        } elseif(Auth::user()->hasRole('vendor')){
            $insert_about_us->description = Auth::user()->id;
        }
        $insert_about_us->save();
        return back()->with('success',true);


    }

    function add_policy(Request $request){
    	$insert_about_us = new Setting();
        $insert_about_us->policy = $request->policy;
       if(Auth::user()->hasRole('admin')){
            $insert_about_us->description = 'admin';
        } elseif(Auth::user()->hasRole('vendor')){
            $insert_about_us->description = Auth::user()->id;
        }
        $insert_about_us->save();
        return back()->with('success',true);
    }


    
}
