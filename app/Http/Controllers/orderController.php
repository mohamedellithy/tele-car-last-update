<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\Order;
use DB;
use App\Models\Product;
use App\User;


use Illuminate\Http\Request;

class orderController extends Controller
{
    function show_order(){
         $show_all_vendor_orders = Order::all();
         return view('vendor.pages.order.index')->with('order_data',$show_all_vendor_orders);
    }

    function change_status(Request $request){

    	Order::where('id',$request->order_id)->update(['status'=>$request->value_status]);

    }
}


 