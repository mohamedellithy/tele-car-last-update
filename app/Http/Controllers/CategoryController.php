<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Image;
use File;


class CategoryController extends Controller
{
    public function index(){
        $categories = Category::with('image')->get();

        return view('admin.pages.categories.index', compact('categories'));
    }

    function handle_category_images($image){
        $orginalNameImage=null;
        if( !empty($image) ){
            $category_image_handle = $image;
            $orginalNameImage = time().'_'.$category_image_handle->getClientOriginalName();
            $category_image_handle->move(public_path('Category_images'),$orginalNameImage);  
        }
         return $orginalNameImage;
    }


    public function add_category_data_type_post(Request $request ){
      
       $this->validate($request,[
                 'Category_name'=>'required|string|max:255',
                 'Category_image'=>'mimes:jpg,png,jpeg'
        ]);
        
        $orginalNameImage = $this->handle_category_images($request->file('Category_image'));

        $add_Category_data   = new Category();
        $add_Category_data->Name   = $request->Category_name;
        $add_Category_data->save();
       // if(!empty($orginalNameImage)){
	        $add_image = new Image();
	        $add_image->imagable_id = $add_Category_data->id;
	        $add_image->imagable_type = 'Category';
	        $add_image->name = $orginalNameImage;
	        $add_image->img_url = asset("Category_images/$orginalNameImage");
	        $add_image->save();
        	
       // }

        return back()->with('success',true);
    }

    public function delete_category_data($category_id){
    	if(!empty(Category::where('id',$category_id)->first()->image->name)){
    	    $image_name = Category::where('id',$category_id)->first()->image->name;
        }
    	$category = Category::where('id',$category_id)->delete();
    	if(!empty($image_name)){
    	    \File::delete(public_path("Category_images/$image_name"));  	
    	}
        return redirect()->route('admin.show-categories')->with('success',true);
    }

    public function delete_selected_category(Request $request){

    	   if(!empty($request->selected_category)){
               $all_selected_category = Category::whereIN('id',$request->selected_category)->get();
               Category::whereIN('id',$request->selected_category)->delete();
               if(!empty($all_selected_category)){
               	   foreach ($all_selected_category as $all_selected_category) {
               	   	    $image_name = $all_selected_category->image->name;
               	   	    if(!empty($image_name)){
				    	    \File::delete(public_path("Category_images/$image_name"));  	
				    	}

               	   }
               }
               return back()->with('success',true);
           }

           return back();
    }

   public function edite_category($category_id){
       // $category_data = Category::where('id',$category_id)->get(); 
        
        return view('admin.pages.categories.add')->with('category_id',$category_id);
    }

    function edite_category_data(Request $request){
        
	        $this->validate($request,[
	                 'Category_name'=>'required|string|max:255',
	                 'Category_image'=>'mimes:jpg,png,jpeg'
	        ]);
	         
	        $check_if_image_not_used_befor=Image::where('imagable_id',$request->Category_id)->pluck('name');
	        if($check_if_image_not_used_befor[0]!=$request->file('Category_image') ){
	          
	            if(empty($request->file('Category_image'))){
	                $image_update = $request->old_value;
	            }
	            elseif(!empty($request->file('Category_image'))){
	            
	                   $image_update = $this->handle_category_images($request->file('Category_image'));
	                  \File::delete(public_path("Category_images/".$check_if_image_not_used_befor[0]));  
	            }
	        }
	        elseif($check_if_image_not_used_befor[0]==$request->file('Category_image') ){
	            $image_update = $check_if_image_not_used_befor[0];
	        }



	        $edite_category_data=Category::where('id',$request->Category_id)->update(['name'=>$request->Category_name]); 
            
	        Image::where('imagable_id',$request->Category_id)->update(['name'=>$image_update,'img_url'=>asset("Category_images/$image_update")]); 
	        
	        return back()->with('success',true);  
    }
}
