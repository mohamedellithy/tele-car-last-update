@extends('admin.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection
@if(!empty($category_id))
@php $category_data_all = App\Models\Category::where('id',$category_id)->get();  @endphp
 @foreach($category_data_all as $category_data )

 @endforeach
 @endif
@section('content')
    <div class="content">
        <div class="container-fluid">

              <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">اضافة تصنيف !</h4>
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                                <li class="breadcrumb-item active">اضافة تصنيف</li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                 
                       <!-- show if have errors -->
                       @if($errors->any())
                         <div class="col-lg-12 alert-danger error_alert">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                         </div>
                        @else
                           <!--  show if have no errors success message -->
                            @if (session('success'))
                                  <div class="col-lg-12 alert-success error_alert">
                                       تمت اضافة القسم بنجاح
                                 </div>
                            @endif

                        @endif

                    
                    <br/>

                    <div class="col-lg-8">

                        <div class="card-box">
                            <h4 class="header-title m-t-0 custom-header">
                               @if(empty($category_data))  
                                  <i class="ion-grid">  </i>  اضافة قسم
                               @else
                                   <i class="ion-compose"> </i> <i class="ion-grid"></i>  تعديل القسم
                               @endif

                            </h4>
                            @if(!empty($category_data)) 
                                <a href="{{ url('admin/delete-category-data/'.$category_data->id) }}" class="btn btn-danger btn_delete_vendor"> <i class="ion-trash-a"></i> حذف القسم</a>
                            @endif
                            <div class="clearfix"></div><hr/>
                            
                            <form action="{{ ( empty($category_data)?route('admin.add-data-category'):route('admin.edite-category-data') ) }}" enctype="multipart/form-data" method="POST" >
                                 {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="Category_name">اسم القسم<span class="text-danger">*</span></label>
                                    <input type="text" name="Category_name" parsley-trigger="change" 
                                        value="{{ ( !empty($category_data)?$category_data->name:'') }}" required
                                           placeholder="اسم القسم" class="form-control" id="Category_name">
                                    @if(!empty($category_data))
                                      <input type="hidden" name="Category_id" value="{{ $category_data->id }}">
                                    @endif
                                </div>
                               
                                <div class="form-group">
                                    <label for="emailAddress">صورة القسم<span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" name="Category_image" >
                                    @if(!empty($category_data))
                                       <input type="hidden" value="{{ (!empty($category_data->image->name)?$category_data->image->name:'no_image.png') }}"  name="old_value">
                                    @endif
                                </div>

                               
                                
                                
                                <div class="form-group text-right m-b-0">
                                   @if(empty($category_data))
                                       <button class="btn btn-primary waves-effect waves-light" type="submit">  انشاء قسم  </button>
                                    @else
                                       <button class="btn btn-primary waves-effect waves-light" type="submit">  تعديل على القسم  </button>
                                   @endif 
                                </div>

                            </form>
                        </div> <!-- end card-box -->
                    </div>
                    <!-- end col -->
                    <div class="col-lg-4 container_image_category">
                            @if(!empty($category_data)) 
                             <div style="margin-bottom:10px;float:left">
                                <a href="{{ url('delete-image-data/'.$category_data->id) }}" class="btn btn-danger btn_delete_vendor"> <i class="ion-trash-a"></i> حذف الصورة</a>
                             </div>
                            @endif
                          <img style="float:left" class="img-responsive" src="{{ asset('Category_images/'.( ( !empty($category_data->image->name) )?$category_data->image->name:'no_image.png')) }} " />
                    </div>
                </div>
                <!-- end row -->
            
           
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection