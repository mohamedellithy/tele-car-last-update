@extends('admin.layouts.master')

@section('title')
Tele-car
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            
                  <!-- Page-Title -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">التجار !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active">اضافة تاجر</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                   <!-- show if have errors -->
                   @if($errors->any())
                     <div class="col-lg-12 alert-danger error_alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                     </div>
                    @else
                       <!--  show if have no errors success message -->
                        @if (session('success'))
                              <div class="col-lg-12 alert-success error_alert">
                                   تمت اضافة التاجر بنجاح
                             </div>
                        @endif

                    @endif

                
                <br/>
                <div class="col-lg-12">

                    <div class="card-box">
                        <h4 class="header-title m-t-0 custom-header">
                           @if(empty($vendor_data))  
                              <i class="ion-person-add">  </i>  اضافة تاجر
                           @else
                               <i class="ion-compose">  </i>  تعديل تاجر
                           @endif

                        </h4>
                        @if(!empty($vendor_data)) 
                            <a href="{{ url('admin/delete-vendor-data/'.$vendor_data->id) }}" class="btn btn-danger btn_delete_vendor"> <i class="ion-trash-a"></i> حذف التاجر</a>
                        @endif
                        <div class="clearfix"></div>
                        <hr/>

                       
                        <form action="{{ ( empty($vendor_data)?route('admin.add-vendor-post'):route('admin.save-edite-vendor-data') ) }}" method="POST">
                             {{ csrf_field() }}

                            <div class="form-group">
                                <label for="userName">اسم المستخدم<span class="text-danger">*</span></label>
                                <input type="text" name="name_vendor" parsley-trigger="change" 
                                    value="{{ ( !empty($vendor_data)?$vendor_data['name']:'') }}" required
                                       placeholder="اسم المستخدم" class="form-control" id="userName">
                                @if(!empty($vendor_data))
                                  <input type="hidden" name="vendor_id" value="{{ $vendor_data->id }}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="emailAddress">البريد الالكترونى<span class="text-danger">*</span></label>
                                <input type="email" value="{{ ( !empty($vendor_data)?$vendor_data['email']:'') }}" name="email" parsley-trigger="change" required
                                       placeholder="البريد الالكترونى" class="form-control" id="emailAddress">
                            </div>
                            @if(empty($vendor_data))
                                <div class="form-group">
                                    <label for="passWord1">كلمة المرور<span class="text-danger">*</span></label>
                                    <input name="password_vendor" value="{{ ( !empty($vendor_data)?$vendor_data['password']:'') }}" data-parsley-equalto="#pass1" type="password" required
                                           placeholder="كلمة المرور" class="form-control" id="passWord1">
                                </div>
                                <div class="form-group">
                                    <label for="passWord2">كلمة المرور<span class="text-danger">*</span></label>
                                    <input name="password_confirmation" data-parsley-equalto="#pass1" type="password" required
                                           placeholder="تأكيد كلمة المرور" class="form-control" id="passWord2">
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="phone_number">رقم التليفون<span class="text-danger">*</span></label>
                                <input name="phone" value="{{ ( !empty($vendor_data)?$vendor_data['phone']:'') }}" data-parsley-equalto="#pass1" type="phone" required 
                                       placeholder="رقم التليفون" class="form-control" id="phone_number">
                            </div>
                            
                            <div class="form-group text-right m-b-0">
                               @if(empty($vendor_data))
                                   <button class="btn btn-primary waves-effect waves-light" type="submit">  انشاء تاجر  </button>
                                @else
                                   <button class="btn btn-primary waves-effect waves-light" type="submit">  تعديل على تاجر  </button>
                               @endif 
                            </div>

                        </form>
                    </div> <!-- end card-box -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

                   
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection