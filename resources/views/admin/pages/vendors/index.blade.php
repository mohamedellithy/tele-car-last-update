@extends('admin.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title"> التجار !</h4>
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                                <li class="breadcrumb-item active">عرض التجار</li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                   <!--  show if have errors  -->
                   @if($errors->any())
                     <div class="col-lg-12 alert-danger error_alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                     </div>
                    @else
                       <!--  show if have no errors success message  -->
                        @if (session('success'))
                              <div class="col-lg-12 alert-success error_alert">
                                   تمت حذف التاجر بنجاح
                             </div>
                        @endif

                    @endif
                   <div class="col-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title">
                                <i class="ion-person-stalker"></i>
                                عرض جميع التجار
                            </h4><hr/>
                           
                            <form method="POST" action="{{ route('admin.delete-selected-vendor') }}">
                                {{csrf_field() }}
                                <button class="btn btn-danger deleted_selected" type="submit"> <i class="ion-trash-a"></i> حذف المحدد </button>

                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>الاسم </th>
                                        <th>البريد الالكترونى</th>
                                        <th>رقم الهاتف</th>
                                        <th>تاريخ الانضمام</th>
                                        
                                        <th>تعديل</th>
                                        <th>حذف</th>
                                        <th>تحديد</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <!--
                                           * @package added by mohamed ellithy
                                           * here show all user have role from type vendor
                                        -->
                                      @php  $all_vendors =App\User::all();  @endphp
                                        <!-- extract all data -->
                                        @foreach ($all_vendors as $vendor) 
                                            <!-- check if users is vendor or not --> 
                                            @if($vendor->hasRole('vendor'))
                                                <tr>
                                                    <td>{{ $vendor->name       }}</td>
                                                    <td>{{ $vendor->email      }}</td>
                                                    <td>{{ $vendor->phone      }}</td>
                                                    <td>{{ $vendor->created_at }}</td>
                                                  
                                                    <td><a href="{{ url('admin/edite-vendor/'.$vendor->id) }}" class="btn btn-info"><i class="ion-compose"></i> تعديل</a> </td>
                                                    <td><a href="{{ url('admin/delete-vendor-data/'.$vendor->id) }}" class="btn btn-danger"><i class="ion-trash-a"></i> حذف</a> </td>
                                                    <td>
                                                       <div class="checkbox checkbox-primary m-r-15">
                                                          <input id="checkbox2{{ $vendor->id}}" value="{{ $vendor->id }}" type="checkbox" name="selected_vendor[]" />
                                                          <label for="checkbox2{{ $vendor->id}}"></label>
                                                       </div>
                                                    </td>
                                                </tr>
                                            @endif <!-- end if -->

                                        @endforeach <!-- end foreach -->
                                        
                                </tbody>
                            </table>
                         </form>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->    
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection