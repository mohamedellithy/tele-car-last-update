<!-- ========== Left Sidebar Start ========== -->
<style type="text/css">
#sidebar-menu a
{
    border-bottom: 1px solid #eee;
}
</style>
<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            <!-- here add all linls in sidebar -->
                            @php $all_links_sidebar=array(
                                        array(
                                             'icon'=>'ti-home',
                                             'name'=>'الرئيسية',
                                             'route'=>'/admin/',
                                        ),
                                        array(
                                             'icon'=>'ion-person-add',
                                             'name'=>'اضافة تجار',
                                             'route'=>'/admin/add-vendors',
                                        ),
                                        array(
                                             'icon'=>'ion-person-stalker',
                                             'name'=>'عرض التجار',
                                             'route'=>'/admin/show-vendors',
                                        ),
                                         array(
                                             'icon'=>'ion-grid',
                                             'name'=>'اضافة تصنيف',
                                             'route'=>'/admin/add-category',
                                        ),
                                         array(
                                             'icon'=>'ion-grid',
                                             'name'=>'عرض تصنيف ',
                                             'route'=>'/admin/show-categories',
                                        ),
                                         array(
                                             'icon'=>'ion-grid',
                                             'name'=>'عرض المستخدمين',
                                             'route'=>'/admin/show-users',
                                        ),
                                         array(
                                             'icon'=>'ti-info-alt',
                                             'name'=>'من نحن ',
                                             'route'=>'/admin/about-us',
                                        ),
                                         array(
                                             'icon'=>'ion-ios7-compose',
                                             'name'=>'الخصوصية ',
                                             'route'=>'/admin/policy',
                                        )

                                 );

                            @endphp

                            @foreach($all_links_sidebar as $key => $value)
                                    <li>
                                        <a href="{{ url($value['route']) }} " class="waves-effect waves-primary"><i
                                                class="{{ $value['icon'] }}"></i><span> {{ $value['name'] }} </span>
                                        </a>
                                    </li>
                                  
                            @endforeach

                            
                        </ul>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->