@extends('admin.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
 <!--Morris Chart CSS -->
<link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/jquery-circliful/css/jquery.circliful.css')}}" rel="stylesheet" type="text/css" />

<script src="{{asset('assets/js/modernizr.min.js')}}"></script>


@endsection

@section('styles')
@endsection

@section('content')
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"> الرئيسية !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Minton</a></li>
                            <li class="breadcrumb-item active"> الرئيسية</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>


            <!-- Page-Title -->
             <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="100%" data-width="5" data-fontsize="14" data-percent="100" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-success counter m-t-10"> {{ App\User::count() }} </h3>
                        <p class="text-muted text-nowrap m-b-10"> عدد المستخدمين </p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="100%" data-width="5" data-fontsize="14" data-percent="100" data-fgcolor="#3bafda" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-primary counter m-t-10"> {{ App\Models\Product::count() }} </h3>
                        <p class="text-muted text-nowrap m-b-10"> عدد المنتجات </p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="100%" data-width="5" data-fontsize="14" data-percent="100" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                           {{ App\Models\Order::count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات</p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        
                        @if( !empty( App\Models\Order::count() ) )
                           $statics_accepted_order = ( ( App\Models\Order::where('status','accepted')->count()  * 100 ) / App\Models\Order::count() );   
                        @endif
                          
                        <div class="circliful-chart" data-dimension="90" data-text="{{ (!empty($statics_accepted_order)?$statics_accepted_order:0).'%'}}" data-width="5" data-fontsize="14" data-percent="{{ (!empty($statics_accepted_order)?$statics_accepted_order:0)}}" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                           {{ App\Models\Order::where('status','accepted')->count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات المقبولة</p>
                    </div>
                </div>


                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                       
                         @if( !empty( App\Models\Order::count() ) )
                              $statics_pending_order = ( ( App\Models\Order::where('status','pending')->count() * 100 ) / App\Models\Order::count() );  
                       
                         @endif
                        <div class="circliful-chart" data-dimension="90" data-text=" {{ (!empty($statics_pending_order)?$statics_pending_order:0).'%'}} " data-width="5" data-fontsize="14" data-percent="{{  (!empty($statics_pending_order)?$statics_pending_order:0) }}" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                          {{ App\Models\Order::where('status','pending')->count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات المنتظرة</p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        @if( !empty( App\Models\Order::count() ) )
                              $statics_refuse_order = ( ( App\Models\Order::where('status','refuse')->count() * 100 ) / App\Models\Order::count() );  
                       
                         @endif
                        <div class="circliful-chart" data-dimension="90" data-text="{{ (!empty($statics_refuse_order)?$statics_refuse_order:0).'%'}}" data-width="5" data-fontsize="14" data-percent="{{ (!empty($statics_refuse_order)?$statics_refuse_order:0)}}" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                          {{ App\Models\Order::where('status','refuse')->count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات المرفوضة</p>
                    </div>
                </div>

                 <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                         @if( !empty( App\Models\Order::count() ) )
                              $statics_cancele_order = ( ( App\Models\Order::where('status','cancele')->count() * 100 ) / App\Models\Order::count() );  
                       
                         @endif
                        <div class="circliful-chart" data-dimension="90" data-text="{{ (!empty($statics_cancele_order)?$statics_cancele_order:0).'%'}}" data-width="5" data-fontsize="14" data-percent="{{ (!empty($statics_cancele_order)?$statics_cancele_order:0)}}" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                          {{ App\Models\Order::where('status','cancele')->count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات الملغية</p>
                    </div>
                </div>

            </div>
            <!-- end row -->
                <!-- BAR Chart -->
                <div class="row">
                  
                    <div class="col-lg-12">
                        <div class="portlet">
                            <!-- /primary heading -->
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark"> تحليل بيانى للمستخدمين لعام ({{date('Y')}}) </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default1"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-default1" class="panel-collapse collapse show">
                                <div class="portlet-body">
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #3bafda;"></i>عدد المستخدمين</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>الشهر</h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="monthlyReport" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>
                    <!-- col -->
                </div>
                <!-- End row-->
                
                
                 <!-- BAR Chart -->
                <div class="row">
                  
                    <div class="col-lg-12">
                        <div class="portlet">
                            <!-- /primary heading -->
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark"> تحليل بيانى للمنتجات لعام ({{date('Y')}}) </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default1"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-default1" class="panel-collapse collapse show">
                                <div class="portlet-body">
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #3bafda;"></i>عدد المنتجات</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>الشهر</h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="monthlyReportproduct" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>
                    <!-- col -->
                </div>
                <!-- End row-->
                
                
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')
<script src="{{asset('plugins/switchery/switchery.min.js')}}"></script>

        <!-- Counter Up  -->
        <script src="{{asset('plugins/waypoints/lib/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('plugins/counterup/jquery.counterup.min.js')}}"></script>

        <!-- circliful Chart -->
        <script src="{{asset('plugins/jquery-circliful/js/jquery.circliful.min.js')}}"></script>
        <script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

        <!-- skycons -->
        <script src="{{asset('plugins/skyicons/skycons.min.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/jquery.dashboard.js')}}"></script>
@endsection
@section('scripts')
<script type="text/javascript">
            var resizefunc = [];
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
                $('.circliful-chart').circliful();
            

            // BEGIN SVG WEATHER ICON
            if (typeof Skycons !== 'undefined'){
                var icons = new Skycons(
                        {"color": "#3bafda"},
                        {"resizeClear": true}
                        ),
                        list  = [
                            "clear-day", "clear-night", "partly-cloudy-day",
                            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                            "fog"
                        ],
                        i;

                for(i = list.length; i--; )
                    icons.set(list[i], list[i]);
                icons.play();
            };
        });

        </script>

<!--Morris Chart-->

<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('plugins/raphael/raphael-min.js')}}"></script>
<script src="{{asset('assets/pages/morris.init.js')}}"></script>

<!-- Custom main Js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

<script> 
    new Morris.Line({
      // ID of the element in which to draw the chart.
      element: 'monthlyReport',
       parseTime: false,
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.
      data: [
        { month: '01',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-01-30 00:13:13'])->count() }}"  },
        { month: '02',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-02-30 00:13:13'])->count() }}"  },
        { month: '03',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-03-30 00:13:13'])->count() }}"  },
        { month: '04',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-04-30 00:13:13'])->count() }}"  },
        { month: '05',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-05-30 00:13:13'])->count() }}" },
        { month: '06',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-06-30 00:13:13'])->count() }}"  },
        { month: '07',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-07-30 00:13:13'])->count() }}"  },
        { month: '08',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-08-30 00:13:13'])->count() }}"  },
        { month: '09',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-09-30 00:13:13'])->count() }}"  },
        { month: '10',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-10-30 00:13:13'])->count() }}" },
        { month: '11',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-11-30 00:13:13'])->count() }}"  },
        { month: '12',  value: "{{ App\User::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-12-30 00:13:13'])->count() }}"  },
       
        
        
      ],
      
      // The name of the data record attribute that contains x-values.
      xkey: 'month',
      // A list of names of data record attributes that contain y-values.
      ykeys: ['value'],
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: ['مستخدم']
    });
    
      new Morris.Line({
      // ID of the element in which to draw the chart.
      element: 'monthlyReportproduct',
       parseTime: false,
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.
      data: [
        { month: '01',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-01-30 00:13:13'])->count() }}"  },
        { month: '02',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-02-30 00:13:13'])->count() }}"  },
        { month: '03',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-03-30 00:13:13'])->count() }}"  },
        { month: '04',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-04-30 00:13:13'])->count() }}"  },
        { month: '05',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-05-30 00:13:13'])->count() }}" },
        { month: '06',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-06-30 00:13:13'])->count() }}"  },
        { month: '07',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-07-30 00:13:13'])->count() }}"  },
        { month: '08',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-08-30 00:13:13'])->count() }}"  },
        { month: '09',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-09-30 00:13:13'])->count() }}"  },
        { month: '10',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-10-30 00:13:13'])->count() }}" },
        { month: '11',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-11-30 00:13:13'])->count() }}"  },
        { month: '12',  value: "{{ App\Models\Product::whereBetween('created_at',[date('y').'-01-01 00:13:13',date('y').'-12-30 00:13:13'])->count() }}"  },
       
        
        
      ],
      
      // The name of the data record attribute that contains x-values.
      xkey: 'month',
      // A list of names of data record attributes that contain y-values.
      ykeys: ['value'],
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: ['منتج']
    });
    
    
</script>



@endsection

