@extends('vendor.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">عرض المنتجات !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active">عرض المنتجات</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
               <!--  show if have errors  -->
               @if($errors->any())
                 <div class="col-lg-12 alert-danger error_alert">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                 </div>
                @else
                   <!--  show if have no errors success message  -->
                    @if (session('success'))
                          <div class="col-lg-12 alert-success error_alert">
                               تمت حذف المستخدم بنجاح
                         </div>
                    @endif

                @endif
                
               <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">
                            <i class="ion-person-stalker"></i>
                            عرض جميع الطلبات
                        </h4><hr/>
                       
                        
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>الاسم المشترى</th>
                                    <th>اسم المنتج</th>
                                    <th>حالة المنتج</th>
                                    <th>تاريخ الطلب</th>
                                    <th>تغير الحالة</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                              @if($order_data)
                                  @foreach($order_data as $order_info)
                                    @if($order_info->custom_products->user_id=Auth::user()->id)
                                       @if(!empty($order_info))
                                            <tr>
                                                    <td>{{ $order_info->user->name }} </td> 
                                                    <td>{{ $order_info->custom_products->name }} </td>
                                                    <td>{{ $order_info->status }} </td>
                                                    <td>{{ $order_info->created_at }} </td>
                                                    <td> 
                                                        <select data-order="{{$order_info->id}}"  class="status_select form-control">
                                                              <option value="pending"  {{ (($order_info->status=='pending')?'selected':'') }} >pending</option>
                                                              <option value="accepted" {{ (($order_info->status=='accepted')?'selected':'') }} >accepted</option>
                                                              <option value="refuse"   {{ (($order_info->status=='refuse')?'selected':'') }} >refuse</option>
                                                              <option value="cancel"   {{ (($order_info->status=='cancel')?'selected':'') }} >cancel</option>

                                                        </select> 
                                                    </td>

                                             </tr> 
                                        @endif
                                    @endif
                                  @endforeach
                              @endif
                                    

                            </tbody>
                        </table>
                    
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $(".status_select").change(function(){
               var order_id_handle = $(this).attr('data-order');
               var value_status_handle =$(this).val(); 
                $.ajax({
                    type:'get',
                    url :"{{url('vendor/change-status')}}",
                    data:{
                        order_id     :order_id_handle,
                        value_status :value_status_handle,
                    },
                    success:function(result){
                       location.reload();
                    }
                })
            });
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection