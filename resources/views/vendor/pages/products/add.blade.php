@extends('vendor.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection
@if(!empty($category_id))
@php $category_data_all = App\Models\Category::where('id',$category_id)->get();  @endphp
 @foreach($category_data_all as $category_data )

 @endforeach
 @endif
@section('content')
    <div class="content">
        <div class="container-fluid">

          <!-- Page-Title -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"> اضافة منتجات !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active"> اضافة منتجات </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                   <!-- show if have errors -->
                   @if($errors->any())
                     <div class="col-lg-12 alert-danger error_alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                     </div>
                    @else
                       <!--  show if have no errors success message -->
                        @if (session('success'))
                              <div class="col-lg-12 alert-success error_alert">
                                   تمت اضافة المنتج بنجاح
                             </div>
                        @endif

                    @endif

                
                <br/>
                <div class="col-lg-8">

                    <div class="card-box">
                        <h4 class="header-title m-t-0 custom-header">
                           @if(empty($product_data))  
                              <i class="ion-person-add">  </i>  اضافة منتج جديد
                           @else
                               <i class="ion-compose">  </i>  تعديل منتج
                           @endif

                        </h4>
                        @if(!empty($product_data)) 
                            <a href="{{ url('vendor/delete-product/'.$product_data->id) }}" class="btn btn-danger btn_delete_vendor"> <i class="ion-trash-a"></i> حذف التاجر</a>
                        @endif
                        <div class="clearfix"></div>
                        <hr/>

                       
                        <form action="{{ ( empty($product_data)?route('vendor.add-products-post'):route('vendor.edite_product_post') ) }}" method="POST" enctype="multipart/form-data" >
                             {{ csrf_field() }}

                            <div class="form-group">
                                <label for="userName">اسم المنتج<span class="text-danger">*</span></label>
                                <input type="text" name="product_name" parsley-trigger="change" 
                                    value="{{ ( !empty($product_data)?$product_data['name']:'') }}" required
                                       placeholder="اسم المنتج" class="form-control" id="userName">
                                @if(!empty($product_data))
                                  <input type="hidden" name="product_id" value="{{ $product_data->id }}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="emailAddress">وصف المنتج<span class="text-danger">*</span></label>
                                <textarea rows="5"  name="full_description"  class="form-control" required>
                                       {{ ( !empty($product_data)?$product_data['details']:'') }}
                                </textarea>
                            </div>
                            
                            <div class="form-group">
                                <label for="passWord1">سعر المنتج<span class="text-danger">*</span></label>
                                <input name="product_price" value="{{ ( !empty($product_data)?$product_data['price']:'') }}" data-parsley-equalto="#pass1"  required
                                       placeholder="سعر المنتج" class="form-control" id="passWord1">
                            </div>
                            
                            <div class="form-group">
                                <label for="passWord1">صورة المنتج<span class="text-danger">*</span></label>
                                <input name="product_image" data-parsley-equalto="#pass1"  type="file"
                                        class="form-control" id="passWord1">
                                
                                @if(!empty($product_data))
                                   <input type="hidden" value="{{ (!empty($product_data->image->name)?$product_data->image->name:'no_image.png') }}"  name="old_value">
                                @endif
                                    
                            </div>
                            
                            <div class="form-group">
                                <label for="passWord2">تصنيف<span class="text-danger">*</span></label>
                                 <select name="product_category" class="form-control">
                                  
                                  @foreach($all_categories as $category)
                                       <option value="{{ $category->id }}" {{ ( ( !empty($product_data['category_id']) && ( $category->id ==$product_data['category_id']))?'selected':'') }} > {{ $category->name }}</option>
                                   
                                  @endforeach
                               </select>
                            </div>
                           
                            
                            
                           
                            <div class="form-group">
                                <label for="phone_number">خصومات<span class="text-danger">*</span></label>
                                <input name="product_discount" value="{{ ( !empty($product_data)?$product_data['discount']:'') }}" data-parsley-equalto="#pass1" type="text"  
                                       placeholder="خصومات" class="form-control" id="phone_number">
                            </div>
                            
                             <div class="form-group">
                                <label for="phone_number">رقم التواصل بالهاتف<span class="text-danger">*</span></label>
                                <input name="call_phone" value="{{ ( !empty($product_data)?$product_data['call_phone']:'') }}" data-parsley-equalto="#pass1" type="text"  
                                       placeholder="رقم الموبايل" class="form-control" id="phone_number">
                            </div>
                            
                             <div class="form-group">
                                <label for="phone_number">رقم التواصل بالواتس<span class="text-danger">*</span></label>
                                <input name="whats_number" value="{{ ( !empty($product_data)?$product_data['whats_number']:'') }}" data-parsley-equalto="#pass1" type="text"  
                                       placeholder="رقم الواتس" class="form-control" id="phone_number">
                            </div>
                            
                            <div class="form-group text-right m-b-0">
                               @if(empty($product_data))
                                   <button class="btn btn-primary waves-effect waves-light" type="submit">  انشاء منتج  </button>
                                @else
                                   <button class="btn btn-primary waves-effect waves-light" type="submit">  تعديل على منتج  </button>
                               @endif 
                            </div>

                        </form>
                    </div> <!-- end card-box -->
                </div>
               <!-- end col -->
                <div class="col-lg-4 container_image_category">
                        @if(!empty($product_data)) 
                        <div style="margin-bottom:10px;float:left">
                            <a href="{{ url('delete-image-data/'.$product_data->id) }}" class="btn btn-danger btn_delete_vendor"> <i class="ion-trash-a"></i> حذف الصورة</a>
                        </div>
                        <br/> 
                           
                        @endif
                      <img style="float:left" class="img-responsive" src="{{ asset('product_images/'.( ( !empty($product_data->image->name) )?$product_data->image->name:'no_image.png')) }} " />
                </div>
          
            </div>
            <!-- end row -->
            
           
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection