@extends('vendor.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"> عرض المنتجات !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active"> عرض المنتجات </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
               <!--  show if have errors  -->
               @if($errors->any())
                 <div class="col-lg-12 alert-danger error_alert">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                 </div>
                @else
                   <!--  show if have no errors success message  -->
                    @if (session('success'))
                          <div class="col-lg-12 alert-success error_alert">
                               تمت حذف المستخدم بنجاح
                         </div>
                    @endif

                @endif
               <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">
                            <i class="ion-person-stalker"></i>
                            عرض جميع المنتجات
                        </h4><hr/>
                       
                        <form method="POST" action="{{ route('vendor.delete-products-selected') }}">
                            {{csrf_field() }}
                            <button class="btn btn-danger deleted_selected" type="submit"> <i class="ion-trash-a"></i> حذف المحدد </button>

                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>الاسم المنتج </th>
                                    <th> صورة المنتج </th>
                                   
                                    <th>تصنيف</th>
                                     <th>سعر المنتج</th>
                                    <th>خصم على المنتج</th>
                                    <th>تاريخ انشاء المنتج</th>
                                    <th>حذف</th>
                                    <th>تعديل</th>
                                    <th>تحديد</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($all_products)
                                  @foreach($all_products as $product)
                                      @role('vendor')
                                                
                                      
                                        <tr>
                                            <td>{{ $product->name }} </td>
                                            <td><img class="img-in-table" src="{{ asset('product_images/'.( ( !empty($product->image->name) )?$product->image->name:'no_image.png')) }} " /></td>
                                            <td>{{( (!empty($product->category->name) )?$product->category->name:'-') }} </td>
                                            <td>{{ $product->price }} </td>
                                            <td>{{ $product->discount }} </td>
                                            <td>{{ $product->created_at }} </td>
                                            <td><a href="{{ url('vendor/edite-product/'.$product->id) }} " class="btn btn-info"><i class="ion-compose"></i> تعديل</a></td>
                                            <td><a href="{{ url('vendor/delete-product/'.$product->id) }} " class="btn btn-danger"><i class="ion-compose"></i> حذف </a></td>
                                            <td>
                                                <div class="checkbox checkbox-primary m-r-15">
                                                        <input  value=" {{ $product->id }} " type="checkbox" name="selected_products[]"  id="checkbox2{{ $product->id }}" type="checkbox">
                                                        <label for="checkbox2{{ $product->id }}"></label>
                                                </div>
                                            </td>

                                        </tr>
                                      @endrole
                                  @endforeach
                              @endif
                                    

                            </tbody>
                        </table>
                     </form>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection