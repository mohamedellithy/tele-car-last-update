@extends('vendor.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('styles')
@endsection

@php  $policy_all_post=App\Models\Setting::where(['description'=>Auth::user()->id,'about_us'=>null])->get(); @endphp
@if( !empty( $policy_all_post ) )
      @foreach($policy_all_post as $policy_post)
          
      @endforeach

@endif

@section('content')
    <div class="content">
        <div class="container-fluid">

               <!-- Page-Title -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title"> الخصوصية !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active"> الخصوصية </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                   <!-- show if have errors -->
                   @if($errors->any())
                     <div class="col-lg-12 alert-danger error_alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                     </div>
                    @else
                       <!--  show if have no errors success message -->
                        @if (session('success'))
                              <div class="col-lg-12 alert-success error_alert">
                                   تمت اضافة الشروط و الخصوصية بنجاح
                             </div>
                        @endif

                    @endif

                
                <br/>
                <div class="col-lg-12">

                    <div class="card-box">
                        <h4 class="header-title m-t-0 custom-header">
                           <i class="ti-info-alt">  </i>  سياسة الخصوصية و الشروط
                        </h4>
                      
                        <div class="clearfix"></div>
                        <hr/>
                        <form action="{{ route('vendor.add-policy') }}" method="POST">
                              {{ csrf_field() }}
                              <div class="form-group">
                                 
                                  
                                  <textarea name="policy" class="form-control" rows="5"> {{ (!empty($policy_post)?$policy_post->policy:'') }}  </textarea>
                              </div>
                              <div class="form-group text-right m-b-0">
                                
                                     <button class="btn btn-primary waves-effect waves-light" type="submit">  تحديث  </button>
                                
                              </div>
                        </form>
                    </div> <!-- end card-box -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
          
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')

        <script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
        <script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/datatables.editable.init.js')}}"></script>
@endsection
@section('scripts')
<script>
            $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
        </script>
@endsection