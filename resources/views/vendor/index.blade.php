@extends('vendor.layouts.master')

@section('title')
Title Here
@endsection

@section('plugin_styles')
<link href="{{asset('plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/jquery-circliful/css/jquery.circliful.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('styles')
@endsection

@section('content')
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">الرئيسية !</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Tele-Car</a></li>
                            <li class="breadcrumb-item active">الرئيسية</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>



            <!-- Page-Title -->
             <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="100%" data-width="5" data-fontsize="14" data-percent="100" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-success counter m-t-10"> {{ App\User::count() }} </h3>
                        <p class="text-muted text-nowrap m-b-10"> عدد المستخدمين </p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="75%" data-width="5" data-fontsize="14" data-percent="75" data-fgcolor="#3bafda" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-primary counter m-t-10">
                             {{ App\Models\Product::where('user_id',Auth::user()->id)->count() }}
                        </h3>
                        <p class="text-muted text-nowrap m-b-10"> عدد المنتجات </p>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="widget-simple-chart text-right card-box">
                        <div class="circliful-chart" data-dimension="90" data-text="0%" data-width="5" data-fontsize="14" data-percent="0" data-fgcolor="#f76397" data-bgcolor="#ebeff2"></div>
                        <h3 class="text-pink m-t-10"> <span class="counter">
                           {{ App\Models\Order::count() }}
                        </span></h3>
                        <p class="text-muted text-nowrap m-b-10">عدد الطلبات</p>
                    </div>
                </div>

               
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="row">
                                    <div class="col-6 text-center">
                                        <canvas id="partly-cloudy-day" width="100" height="100"></canvas>
                                    </div>
                                    <div class="col-6">
                                        <h2 class="m-t-0 text-muted"><b>32°</b></h2>
                                        <p class="text-muted">Partly cloudy</p>
                                        <p class="text-muted mb-0">15km/h - 37%</p>
                                    </div>
                                </div><!-- End row -->
                            </div>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-4 text-center">
                                        <h4 class="text-muted m-t-0">SAT</h4>
                                        <canvas id="cloudy" width="35" height="35"></canvas>
                                        <h4 class="text-muted">30<i class="wi wi-degrees"></i></h4>
                                    </div>
                                    <div class="col-4 text-center">
                                        <h4 class="text-muted m-t-0">SUN</h4>
                                        <canvas id="wind" width="35" height="35"></canvas>
                                        <h4 class="text-muted">28<i class="wi wi-degrees"></i></h4>
                                    </div>
                                    <div class="col-4 text-center">
                                        <h4 class="text-muted m-t-0">MON</h4>
                                        <canvas id="clear-day" width="35" height="35"></canvas>
                                        <h4 class="text-muted">33<i class="wi wi-degrees"></i></h4>
                                    </div>
                                </div><!-- end row -->
                            </div>
                        </div><!-- end row -->
                    </div>
                </div> <!-- end col -->
            </div>
            
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->
@endsection

@section('plugin_scripts')
<script src="{{asset('plugins/switchery/switchery.min.js')}}"></script>

        <!-- Counter Up  -->
        <script src="{{asset('plugins/waypoints/lib/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('plugins/counterup/jquery.counterup.min.js')}}"></script>

        <!-- circliful Chart -->
        <script src="{{asset('plugins/jquery-circliful/js/jquery.circliful.min.js')}}"></script>
        <script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

        <!-- skycons -->
        <script src="{{asset('plugins/skyicons/skycons.min.js')}}" type="text/javascript"></script>

        <!-- Page js  -->
        <script src="{{asset('assets/pages/jquery.dashboard.js')}}"></script>
@endsection
@section('scripts')
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
                $('.circliful-chart').circliful();
            });

            // BEGIN SVG WEATHER ICON
            if (typeof Skycons !== 'undefined'){
                var icons = new Skycons(
                        {"color": "#3bafda"},
                        {"resizeClear": true}
                        ),
                        list  = [
                            "clear-day", "clear-night", "partly-cloudy-day",
                            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                            "fog"
                        ],
                        i;

                for(i = list.length; i--; )
                    icons.set(list[i], list[i]);
                icons.play();
            };

        </script>
@endsection