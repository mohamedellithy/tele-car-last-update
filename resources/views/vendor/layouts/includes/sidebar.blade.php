<!-- ========== Left Sidebar Start ========== -->
<style type="text/css">
#sidebar-menu a
{
    border-bottom: 1px solid #eee;
}
</style>
<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            <!-- here add all linls in sidebar -->
                            @php  $all_links_sidebar=array(
                                        array(
                                             'icon'=>'ti-home',
                                             'name'=>'الرئيسية',
                                             'route'=>'/vendor/dashboard',
                                        ),
                                        array(
                                             'icon'=>'ion-android-sort',
                                             'name'=>'اضافة منتجات',
                                             'route'=>'/vendor/add-products',
                                        ),
                                        array(
                                             'icon'=>'ion-ios7-keypad',
                                             'name'=>'عرض المنتجات',
                                             'route'=>'/vendor/show-products',
                                        ),
                                        array(
                                             'icon'=>'ion-ios7-cart',
                                             'name'=>'عرض الطلبات',
                                             'route'=>'/vendor/show-orders',
                                        ),
                                        array(
                                             'icon'=>'ion-person-stalker',
                                             'name'=>'عرض المستخدمين',
                                             'route'=>'/vendor/show-users',
                                        ),
                                        array(
                                             'icon'=>'ti-info-alt',
                                             'name'=>'من نحن ',
                                             'route'=>'/vendor/about-us',
                                        ),
                                         array(
                                             'icon'=>'ion-ios7-compose',
                                             'name'=>'الخصوصية  ',
                                             'route'=>'/vendor/policy',
                                        )
                                );

                            @endphp

                            @foreach($all_links_sidebar as $key => $value)
                                    <li>
                                        <a href="{{ url($value['route']) }} " class="waves-effect waves-primary"><i
                                                class="{{ $value['icon'] }}"></i><span> {{ $value['name'] }} </span>
                                        </a>
                                    </li>
                                  
                            @endforeach

                        </ul>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->