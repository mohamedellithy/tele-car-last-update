<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>{{ config('app.name', 'Laravel') }}</title>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
            <meta name="author" content="Coderthemes">

            <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }} ">

            <title>Minton - Responsive Admin Dashboard Template</title>

            <link href="{{ asset('plugins/switchery/switchery.min.css') }}" rel="stylesheet" />

            <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
             <link href='https://fonts.googleapis.com/earlyaccess/droidarabickufi.css' rel="stylesheet">
            <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">

            <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
             @yield('content')
     </div>
   <script>
            var resizefunc = [];
        </script>

        <!-- Plugins  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        
        <script src="{{ asset('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ asset('plugins/switchery/switchery.min.js') }}"></script>

        <!-- Custom main Js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>
        <script src="{{ asset('assets/js/custom.js') }}"></script>
    
    </body>
</html>