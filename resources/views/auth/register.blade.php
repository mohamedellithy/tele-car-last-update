@extends('layouts.app')

@section('content')

        <div class="wrapper-page">

            <div class="text-center">
                <a href="index.html" class="logo-lg"><i class="mdi mdi-radar"></i> <span>Minton</span> </a>
            </div>

          
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                 <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-email"></i></span>
                        </div>
                        <input class="form-control" type="text" required="" name="username" value="{{ old('username') }}"  placeholder="username">
                        @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                        </div>
                        <input class="form-control" type="email" name="email" required="" placeholder="Email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

               <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-key"></i></span>
                        </div>
                        <input class="form-control" type="password" name="password" required="" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>        

                <div class="form-group">
                    <div class="checkbox checkbox-primary text-left">
                        <input id="checkbox-signup" type="checkbox" checked="checked">
                        <label for="checkbox-signup">
                            I accept <a href="#">Terms and Conditions</a>
                        </label>
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <button class="btn btn-primary btn-custom waves-effect waves-light w-md" type="submit">Register</button>
                </div>

                <div class="form-group m-t-30">
                    <div class="text-center">
                        <a href="{{ route('login') }}" class="text-muted">Already have account?</a>
                    </div>
                </div>

            </form>
        </div>

  
@endsection
