@extends('layouts.app')

@section('content')

         <div class="wrapper-page">
            <div class="text-center">
                <a href="index.html" class="logo-lg"><i class="mdi mdi-radar"></i> <span> Tele-Car </span> </a>
            </div>

           <!--  <form class="form-horizontal m-t-20" action="index.html"> -->
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                        </div>
                        <input class="form-control" type="text" required="" name="email" value="{{ old('email') }}" placeholder="البريد الالكترونى">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                    <div class="col-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="mdi mdi-key"></i></span>
                            </div>
                            <input class="form-control" type="password" name="password" required="" placeholder="كلمة المرور">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group text-left">
                    <div class="checkbox checkbox-primary">
                        <input id="checkbox-signup" type="checkbox">
                        <label for="checkbox-signup">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> تذكرنى
                        </label>
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit"> تسجيل الدخول </button>
                </div>

                <div class="form-group row m-t-30">
                    <div class="col-sm-7 text-left">
                       
                       
                    </div>
                   
                </div>
            </form>
        </div>
@endsection
