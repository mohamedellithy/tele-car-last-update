<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/person/login', 'Api\PersonController@login');
Route::post('/person/register', 'Api\PersonController@register');

Route::group(['middleware' => ['auth:api','role:vendor'] ], function () {
    // Requests of vendor
    Route::get('/merchants/product/get', 'Api\ProviderController@myProducts');
    Route::post('/merchants/product/delete', 'Api\ProviderController@deleteProduct');
    Route::post('/merchants/product/add', 'Api\ProviderController@addProduct');
    Route::get('/merchants/counts/get', 'Api\ProviderController@countOrders');
    Route::get('/providers/open-orders', 'Api\ProviderController@openOrders');
    
    // Requests in notifications
    Route::post('/merchants/notifiy/user', 'Api\notificationController@send_notifiy_to_user');
   // Route::get('/get/notifications/{user_id}','Api\notificationController@get_notification');
    
});   

Route::group(['middleware' => ['auth:api'] ], function () {
    // Requests in notifications
    Route::post('/users/notifiy/merchant', 'Api\notificationController@send_notifiy_to_merchant');
    //Route::get('/get/notifications/{user_id}','Api\notificationController@get_notification');
});

Route::get('/users/notifications/read/{notifiy_id}','Api\notificationController@read_notification');

Route::get('/users/get/notifications','Api\notificationController@get_all_notifications');




/* add new routes*/
Route::get('/user/search/{search}','Api\ProviderController@getSearchWords')->where('search', '(.*)');

// Requests of users
Route::get('/users/get/product/by/discount', 'Api\CustomerController@getProductByDiscount');
Route::get('/users/get/product/by/views', 'Api\CustomerController@getProductByViews');
//Route::get('/users/get/notifications', 'Api\CustomerController@notifications');
Route::get('/users/get/product/by/cat/{id}', 'Api\ProviderController@categoryProducts');
Route::get('/admin/cats/get', 'Api\ProviderController@allCategories');

//requests of cities
Route::get('/admin/cities','Api\ProviderController@getcities');
