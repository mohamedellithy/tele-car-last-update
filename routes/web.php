<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth', 'role:user'] ], function() {
  
});

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth', 'role:admin'] ], function() {
     Route::get('/', 'HomeController@admin')->name('home');
     //Route::resource('/categories', 'CategoryController');

      /* add vendors and edite and delete */
      Route::get('/add-vendors', function () { return view('admin.pages.vendors.add'); })->name('add-vendors');
      Route::post('/add-data-vendors','adminController@add_vendor_data_type_post')->name('add-vendor-post');
      Route::get('/show-vendors', function () { return view('admin.pages.vendors.index'); })->name('show-vendors');
      Route::get('/edite-vendor/{vendor_id}','adminController@edite_vendor_data')->name('edite-vendor');
      Route::post('/save-edite-vendor','adminController@edite_vendor_data_post_type')->name('save-edite-vendor-data');
      Route::get('/delete-vendor-data/{vendor_id}','adminController@delete_vendor_data')->name('delete-vendor-data');
      Route::post('/delete-selected-vendor','adminController@delete_selected_vendor')->name('delete-selected-vendor');

      /* add Categories and edite and delete */
       Route::get('/add-category', function () { return view('admin.pages.categories.add'); })->name('add-category');
       Route::post('/add-data-category','CategoryController@add_category_data_type_post')->name('add-data-category');
       Route::get('/show-categories', function () { return view('admin.pages.categories.index'); })->name('show-categories');
       Route::get('/delete-category-data/{category_id}','CategoryController@delete_category_data')->name('delete-category-data');
       Route::post('/delete-selected-category','CategoryController@delete_selected_category')->name('delete-selected-category');
       Route::get('/edite-category/{category_id}','CategoryController@edite_category')->name('edite-category');
       Route::post('/edite-category-data','CategoryController@edite_category_data')->name('edite-category-data');


       /* show users add - edite - delete */
       Route::get('/show-users','userController@show_all_users')->name('show-users');
       Route::get('/delete-user/{user_id}','userController@delete_user')->name('delete-user');
  	   Route::post('/delete-users-selected', 'userController@delete_users_selected' )->name('delete-users-selected');
  	   Route::get('/edite-user/{user_id}', 'userController@edite_user_data' )->name('edite-user');
	  // Route::post('/edite-user-data', 'userController@edite_user_data_type_post' )->name('edite-user-data');

	   /* add settings about-us policies */
	   Route::get('/about-us', function () { return view('admin.pages.settings.about-us'); })->name('about-us');
	   Route::post('/add-about-us', 'settingController@add_about_us' )->name('add-about-us');
	   Route::get('/policy', function () { return view('admin.pages.settings.policies'); })->name('policy');
	   Route::post('/add-policy', 'settingController@add_policy' )->name('add-policy');
	  
});

Route::group(['prefix'=>'vendor','as'=>'vendor.','middleware'=>['auth', 'role:vendor'] ], function() {
     //Route::get('/', 'HomeController@vendor')->name('vendor.home');
     //Route::get('/', 'HomeController@vendor')->name('vendor.home');
     Route::get('/dashboard', function(){ return view('vendor.index'); })->name('dashboard');

     /* show products add - update - delete */
     Route::get('/show-products','productsController@show_products')->name('show-products');
     Route::get('/add-products', 'productsController@add_products')->name('add-products');
     Route::post('/add-products-post', 'productsController@add_products_post_type')->name('add-products-post');
     Route::get('/delete-product/{product_id}','productsController@delete_product_data')->name('delete-product');
     Route::get('/edite-product/{product_id}','productsController@edite_product')->name('edite-product');
     Route::post('edite_product_post','productsController@edite_product_post')->name('edite_product_post');
     Route::post('delete-products-selected','productsController@delete_products_selected')->name('delete-products-selected');
    
    /* show products show users only */
    Route::get('/show-users','userController@show_users_for_vendor')->name('show-users');
    Route::get('/about-us','userController@show_users_for_vendor')->name('show-users');

    /* add settings about-us policies */
     Route::get('/about-us', function () { return view('vendor.pages.settings.about-us'); })->name('about-us');
     Route::post('/add-about-us', 'settingController@add_about_us' )->name('add-about-us');
     Route::get('/policy', function () { return view('vendor.pages.settings.policies'); })->name('policy');
     Route::post('/add-policy', 'settingController@add_policy' )->name('add-policy');

     /** show vendors order **/
    
    Route::get('show-orders','orderController@show_order')->name('show-orders');   
    Route::get('change-status','orderController@change_status')->name('change_status');
    
    
   



});
 
 /* delete image for product and category */ 
Route::get('delete-image-data/{type_id}','productsController@delete_image_data')->name('delete-image-data');
 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
