<?php

use App\User;
use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = Role::where('name', '=', 'User')->first();
        $adminRole = Role::where('name', '=', 'Admin')->first();
        $vendorRole = Role::where('name', '=', 'Vendor')->first();
        $permissions = Permission::all();

        /*
         * Add Users
         *
         */
        if (User::where('email', '=', 'admin@app.com')->first() === null) {
            $newUser = User::create([
                'name'     => 'Admin',
                'email'    => 'admin@app.com',
                'password' => bcrypt('123456'),
            ]);

            $newUser->attachRole($adminRole);
            foreach ($permissions as $permission) {
                $newUser->attachPermission($permission);
            }
        }
         
        if (User::where('email', '=', 'vendor@app.com')->first() === null) {
            $newUser = User::create([
                'name'     => 'Vendor',
                'email'    => 'vendor@app.com',
                'password' => bcrypt('123456'),
            ]);

            $newUser->attachRole($vendorRole);
            foreach ($permissions as $permission) {
                $newUser->attachPermission($permission);
            }
        }

        if (User::where('email', '=', 'user@app.com')->first() === null) {
            $newUser = User::create([
                'name'     => 'User',
                'email'    => 'user@app.com',
                'password' => bcrypt('123456'),
            ]);

            $newUser;
            $newUser->attachRole($userRole);
        }
    }
}
